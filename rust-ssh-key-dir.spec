%define debug_package %{nil}
%bcond_without check
%global __cargo_skip_build 0

%global crate ssh-key-dir

Name:           rust-%{crate}
Version:        0.1.5
Release:        1
Summary:        sshd AuthorizedKeysCommand to read ~/.ssh/authorized_keys.d

License:        Apache-2.0
URL:            https://crates.io/crates/ssh-key-dir
Source:         %{crate}-%{version}.crate
Source1:        https://github.com/coreos/%{crate}/releases/download/v%{version}/%{crate}-%{version}-vendor.tar.gz


BuildRequires:  rust-packaging >= 21

%global _description %{expand:
sshd AuthorizedKeysCommand to read key files from ~/.ssh/authorized_keys.d.}

%description %{_description}

%package     -n %{crate}
Summary:          %{summary}
License:          Apache-2.0 and MIT
Requires:         openssh-server
Requires(post):   openssh-server
Requires(post):   systemd
Requires(postun): openssh-server
Requires(postun): systemd

%description -n %{crate} %{_description}

%files       -n %{crate}
%{_libexecdir}/ssh-key-dir
%config(noreplace) %{_sysconfdir}/ssh/sshd_config.d/40-ssh-key-dir.conf
%doc README.md
%license LICENSE

%post        -n %{crate}
/usr/bin/systemctl try-reload-or-restart sshd.service

%postun      -n %{crate}
if [ $1 == 0 ] ; then
    /usr/bin/systemctl try-reload-or-restart sshd.service
fi

%prep
%autosetup -n %{crate}-%{version} -p1
%cargo_prep

tar xvf %{SOURCE1}
mkdir -p .cargo
cat >.cargo/config << EOF
[source.crates-io]
replace-with = "vendored-sources"

[source.vendored-sources]
directory = "vendor"
EOF

%build
%cargo_build

%install
%cargo_install
mkdir -p  %{buildroot}%{_libexecdir}
mv %{_builddir}/ssh-key-dir-0.1.5/.cargo/bin/ssh-key-dir %{buildroot}%{_libexecdir}
install -Dpm0644 -t %{buildroot}%{_sysconfdir}/ssh/sshd_config.d conf/40-ssh-key-dir.conf

%if %{with check}
%check
%cargo_test
%endif

%changelog
* Thu Nov 28 2024 dongjiao <dongjiao@kylinos.cn> - 0.1.5-1
- Update package version to 0.1.5
  Replace deprecated dependency users by uzers

* Thu Apr 18 2024 lijian <lijian2@kylinos.cn> - 0.1.4-4
- Fix build error, I9HLT0

* Fri Dec 09 2022 liukuo <liukuo@kylinos.cn> - 0.1.4-3
- License compliance rectification

* Fri Dec 09 2022 chendexi <chendexi@kylinos.cn> - 0.1.4-2
- Fix unsafe compile option STRIP

* Tue Nov 08 2022 duyiwei <duyiwei@kylinos.cn> - 0.1.4-1
- upgrade version to 0.1.4

* Tue Jun 07 2022 duyiwei <duyiwei@kylinos.cn> - 0.1.3-1
- upgrade to 0.1.3

* Wed Jan 26 2022 duyiwei <duyiwei@kylinos.cn> - 0.1.2-1
- Package init
