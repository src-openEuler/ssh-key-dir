# ssh-key-dir

#### 介绍
ssh-key-dir is an sshd AuthorizedKeysCommand that reads SSH authorized key files from a directory, ~/.ssh/authorized_keys.d. It allows SSH keys to be managed by multiple tools and processes, without competing over ~/.ssh/authorized_keys.

ssh-key-dir reads key files in lexigraphical order, ignoring any filenames starting with a dot.


#### 安装教程

Install ssh-key-dir rpm package:

yum install ssh-key-dir


You can also install just the ssh-key-dir binary with Rust's Cargo package manager:

cargo install ssh-key-dir

#### 使用说明

ssh-key-dir 是云底座操作系统NestOS的必需组件
#### 参与贡献

master分支使用最新的上游版本，如果检测到上游有最新版本发布，先形成issue后再提交对应PR更新，流程如下。
1.  提交issue
2.  Fork 本仓库
3.  新建 Feat_xxx 分支
4.  提交代码
5.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
